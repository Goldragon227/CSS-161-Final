
public class Dungeon {
	//Stores relevent information about dungeons
	
	private boolean _isComplete;
	private boolean _isBoss;
	
	private int _numMonsters;
	private int _levelMonsters;
	
	private String _name;
	
	Dungeon(int numMonsters, int levelMonsters, String Name){
		setNumMonsters(numMonsters);
		setLevelMonsters(levelMonsters);
		setName(Name);
		setIsComplete(false);
	} 
	
	public boolean getIsComplete() {
		return _isComplete;
	}

	public void setIsComplete(boolean _isComplete) {
		this._isComplete = _isComplete;
	}

	public boolean getIsBoss() {
		return _isBoss;
	}

	public void setIsBoss(boolean _isBoss) {
		this._isBoss = _isBoss;
	}

	public int getNumMonsters() {
		return _numMonsters;
	}

	public void setNumMonsters(int _numMonsters) {
		this._numMonsters = _numMonsters;
	}

	public int getLevelMonsters() {
		return _levelMonsters;
	}

	public void setLevelMonsters(int _levelMonsters) {
		this._levelMonsters = _levelMonsters;
	}

	public String getName() {
		return _name;
	}

	public void setName(String _name) {
		this._name = _name;
	}
}
