
public class ItemDatabase {
	//A class to store items names at the index in a array 
	private String[] _items = new String[500]; 
	
	public void addItem(int itemID, String name){
		this._items[itemID] = name;
	}
	
	public String getItem(int itemID){
		return this._items[itemID];
	}
}
