import java.util.Scanner;

public class MenuSystem {
	MenuSystem(){
		
	}
	
	public void printIntro() {
		//Prints the intro dialogue with the fairy
		Scanner userInput = new Scanner(System.in);
		String nameNPC = "Tutorial Fairy: ";
		System.out.println(nameNPC + " Welcome to Dungeon Clearer Prototype! Name in progress!");
		dialougePause();
		System.out.println(nameNPC + " Here you enter the dungeon and clear out rooms!");
		dialougePause();
		System.out.println(nameNPC + " Killing a monster gives you gold which you can use to buy new items! \n Dont worry about dying, "
				+ "I will just revive you, and you won't loose anything!! ");
		dialougePause();
		System.out.println(nameNPC + " Good luck!");
		System.out.println("The " + nameNPC + " diappears.");
		
	}
	
	private void dialougePause() {
		Scanner userInput = new Scanner(System.in);
		//Used to pause the dialogue so the player is not slammed with a wall of text.
		System.out.println("Type any key and press enter to continue.");
		userInput.next();
	}
	
	public void printTownMenu(Player player){
		Scanner userInput = new Scanner(System.in);
		System.out.println("\nYou are in the main town.");
	}
	//TOWN: 0, SHOP: 1, Inventory: 2, Dungeon Selection: 3, Player Stats: 4 DUNGEON_1_1: 11, DUNGEON_1_2: 12, COMBAT: 100};
	
	public void printNoobTownShop(Shop noobShop, Player player, ItemDatabase items){
	//Prints out the town shop inventory and then asks the player if they want to buy a item
		Scanner userInput = new Scanner(System.in);
		System.out.println("\nYou are in the Noob Town Shop.");
		noobShop.printInventory(3);
		noobShop.shopItem(player,items);
		
	}
	//TOWN: 0, SHOP: 1, Inventory: 2, Dungeon Selection: 3, Player Stats: 4 DUNGEON_1_1: 11, DUNGEON_1_2: 12, COMBAT: 100};
	public void printMovement(Player player){
		int location = player.getCurrentLocation();
		
		System.out.println("\nEnter corrisponding button for; ");
		if (!(location == 0)){
			System.out.println("T: Town, ");
		}
		
		if (!(location == 1)){
			System.out.println("S: Shop, ");
		}
		
		if (!(location == 2)){
			System.out.println("I: Inventory, ");
		}
		
		if (!(location == 3)){
			System.out.println("D: Dungeon, ");
		}
		
		if (!(location == 4)){
			System.out.println("P: Player Stats, ");
		}
		
		Scanner userInput = new Scanner(System.in);
		char input;
		boolean isInvalidInput = true;
		
		while (isInvalidInput){
			input = userInput.next().charAt(0);
			if (!(location == 0) && (input == 'T' || input == 't')){
				player.setLastLocation(location);
				player.setCurrentLocation(0);
				isInvalidInput = false;
				
			} else if (!(location == 1) && (input == 'S' || input == 's')){
				player.setLastLocation(location);
				player.setCurrentLocation(1);
				isInvalidInput = false;
				
			} else if (!(location == 2) && (input == 'I' || input == 'i')){
				player.setLastLocation(location);
				player.setCurrentLocation(2);
				isInvalidInput = false;
				
			} else if (!(location == 3) && (input == 'D' || input == 'd')){
				player.setLastLocation(location);
				player.setCurrentLocation(3);
				isInvalidInput = false;
				
			} else if (!(location == 4) && (input == 'P' || input == 'p')){
				player.setLastLocation(location);
				player.setCurrentLocation(4);
				isInvalidInput = false;
				
			} else {
				System.out.println("Invalid Input");
			}
		}
	}
	
	public void printDungeonSelection(Player player, Dungeon dungeon11){
	//Used to have the player choose the dungeon they want to go to
		Scanner userInput = new Scanner(System.in);
		int input;
		boolean isInvalidInput = true;
		
		System.out.println("Dungeon 1-1: " + dungeon11.getIsComplete());
		
		System.out.println("Enter the two numbers of the dungeon name to go to that dungeon. 11 for 1-1");
		System.out.println("Enter -1 to return to town.");
		//System.out.println("To unlock more dungeons you must fist complete the previous one.");
		
		while (isInvalidInput){
			input = userInput.nextInt();
			switch (input){
			case 11:
				player.setLastLocation(player.getCurrentLocation());
				player.setCurrentLocation(11);
				isInvalidInput = false;
				break;
			case -1:
				player.setLastLocation(player.getCurrentLocation());
				player.setCurrentLocation(0);
				isInvalidInput = false;
			default:
				break;
			}
		}
	}
	
	public void printPlayerStats(Player player){
	//Prints out to the console the player's only functioning stats 
		System.out.println(player.getName());
		System.out.println("HP: " + player.getHP() + " / " + player.getMaxHP());
		System.out.println("Attack: " + player.getAttack());
		System.out.println("Defense: " + player.getDefense());
		System.out.println("Gold: " + player.getGold());
	}

	public void initilizePlayer(Player player){
		//Used to have the user input a name for the player
		Scanner userInput = new Scanner(System.in);
		boolean isDone = true;
		
		//Enter player name
		System.out.println("Enter the name of your character.");
		
		while (isDone){
			String input = userInput.next();
			player.setName(input);
			
			System.out.println("Is " + input + " what you want to be your name?");
			System.out.println("Type y to continue and n to enter a new name.");
			isDone = !checkIfDone();
		}
	}
	
	public boolean checkIfDone(){
		/*Checks if the player inputs y/n and will return true if y/Y and false if n/N 
		 * */
		Scanner userInput = new Scanner(System.in);
		boolean isValidInput = true;
		String input = "";
		
		while(isValidInput){
			boolean isInvalidInput = true;
			
			input = "n";
			
			while (isInvalidInput){
				input = userInput.next();
				
				if (input.length() > 1){
					System.out.println("Invalid input. Please enter a single char; n/N for no and y/Y for yes.");
					
				} else {
					switch(input.charAt(0)){
					case 'y':
					case 'Y':
						isInvalidInput = false;
						break;
					case 'n':
					case 'N':
						isInvalidInput = false;
						break;
					default:
						System.out.println("Invalid input. Please enter n/N for no and y/Y for yes.");
						break;
					}
				}
			}

			switch(input.charAt(0)){
				case 'y':
				case 'Y':
					return true;
				case 'n':
				case 'N':
					return false;
			default:
					return false;
			}
		}
		return false;
	}
}
