
public class Monster {

	/*This class stores the values for the respective monsters
	 * 
	 */
	
	private String _name;
	private int _level;
	private int _ID;
	private int _HP;
	private int _maxHP;
	private int _MP;
	private int _maxMP;
	private int _attack;
	private int _defense;
	private int _magicAttack;
	private int _magicDefense;
	
	private int _goldDrop;
	
	Monster(String name, int HP, int level, int attack, int defense, int magicAttack, int magicDefense){
		setName(name);
		setLevel(level);
		setHP(HP);
		setMaxHP(HP);
		setAttack(attack);
		setDefense(defense);
		setGoldDrop(10);
		setMagicAttack(magicAttack);
		setMagicDefense(magicDefense);
	}
	
	
	//Getters
	public String getName() {
		return _name;
	}
	
	public int getID() {
		return _ID;
	}
	
	public int getHP() {
		return _HP;
	}
	
	public int getMaxHP() {
		return _maxHP;
	}
	
	public int getAttack() {
		return _attack;
	}
	
	public int getDefense() {
		return _defense;
	}
	
	public int getMagicAttack() {
		return _magicAttack;
	}
	
	public int getMagicDefense() {
		return _magicDefense;
	}
	
	public int getGoldDrop() {
		return _goldDrop;
	}
	
	//Magic power not used left in for future use
	public void setMP(int _MP) {
		this._MP = _MP;
	}
	
	public int getMaxMP() {
		return _maxMP;
	}

	

	//Setters
	public void setName(String _name) {
		this._name = _name;
	}

	public void setID(int _ID) {
		this._ID = _ID;
	}

	public void setHP(int _HP) {
		this._HP = _HP;
	}
	
	public void setMaxHP(int _maxHP) {
		this._maxHP = _maxHP;
	}

	public void setAttack(int _attack) {
		this._attack = _attack;
	}

	public void setDefense(int _defense) {
		this._defense = _defense;
	}

	public void setMagicAttack(int _magicAttack) {
		this._magicAttack = _magicAttack;
	}

	public void setMagicDefense(int _magicDefense) {
		this._magicDefense = _magicDefense;
	}

	public void setGoldDrop(int _goldDrop) {
		this._goldDrop = _goldDrop;
	}
	
	
	//Magic power not used left in for future use
	public int getMP() {
		return _MP;
	}
	public void setMaxMP(int _maxMP) {
		this._maxMP = _maxMP;
	}


	public int getLevel() {
		return _level;
	}


	public void setLevel(int _level) {
		this._level = _level;
	}
}
