import java.util.Scanner;

public class Main {
	public static void main(String[] args){
		Scanner userInput = new Scanner(System.in);
		MenuSystem menu = new MenuSystem();
		
		boolean isGameRunning = true;
		
		/*Initialize Items
		*String name, int ID, int attack, int defense, int magicAttack, int magicDefense
		*Non-Consumables 1-99
		*/
		
		//TODO add a way to add items and monsters to the game from files?
		
		//Tier 1 Armour
		Item swordBronze = new Item(      "Bronze Sword",      1, 1, 0, 0, 0);
		Item shieldBronze = new Item(     "Bronze Shield",     2, 0, 1, 0, 0);
		
		Item helmetBronze = new Item(     "Bronze Helmet",     3, 0, 1, 0, 0);
		Item chestplateBronze = new Item( "Bronze Chestplate", 4, 0, 3, 0, 0);
		Item leggingsBronze = new Item(   "Bronze Leggings",   5, 0, 2, 0, 0);
		Item bootsBronze = new Item(      "Bronze Boots",      6, 0, 1, 0, 0);
		
		//Tier 2 Armour
		Item swordIron = new Item(        "Iron Sword",        7, 3, 0, 0, 0);
		Item shieldIron = new Item(       "Iron Shield",       8, 0, 3, 0, 0);
		
		Item helmetIron = new Item(       "Iron Helmet",       9,  0, 3, 0, 0);
		Item chestplateIron = new Item(   "Iron Chestplate",   10, 0, 6, 0, 0);
		Item leggingsIron = new Item(     "Iron Leggings",     11, 0, 5, 0, 0);
		Item bootsIron = new Item(        "Iron Boots",        12, 0, 2, 0, 0);
		
		//Tier 3 Armour
		Item swordSteel = new Item(       "Steel Sword",       13, 10,  0, 0, 0);
		Item shielSteel = new Item(       "Steel Shield",      14,  0, 10, 0, 0);
		
		Item helmetSteel = new Item(      "Steel Helmet",      15,  0, 10, 0, 0);
		Item chestplateSteel = new Item(  "Steel Chestplate",  16,  0, 30, 0, 0);
		Item leggingsSteel = new Item(    "Steel Leggings",    17,  0, 20, 0, 0);
		Item bootsSteel = new Item(       "Steel Boots",       18,  0, 10, 0, 0);
		
		
		/*Consumables ItemID 100+
		 * HP potions: String name, int ID,  int restoreHPValue
		 */
		Item potionHealthLow = new Item(   "Healh Potion Low",  100, 100);
		Item potionHealthMedium = new Item("Healh Potion Low",  101, 300);
		Item potionHealthHigh = new Item(  "Healh Potion Low",  102, 1000);
		
		//Add items to Item Database for name reference base off of IDs
		ItemDatabase itemDatabase = new ItemDatabase();
		
		itemDatabase.addItem(swordBronze.getID(), swordBronze.getName());
		itemDatabase.addItem(shieldBronze.getID(), shieldBronze.getName());
		
		itemDatabase.addItem(helmetBronze.getID(), helmetBronze.getName());
		itemDatabase.addItem(chestplateBronze.getID(), chestplateBronze.getName());
		itemDatabase.addItem(leggingsBronze.getID(), leggingsBronze.getName());
		itemDatabase.addItem(bootsBronze.getID(), bootsBronze.getName());
		
		itemDatabase.addItem(potionHealthLow.getID(), potionHealthLow.getName());
		itemDatabase.addItem(potionHealthMedium.getID(), potionHealthMedium.getName());
		itemDatabase.addItem(potionHealthHigh.getID(), potionHealthHigh.getName());
		
		
		
		//Initialize Player
		
		Player player = new Player();
		
		/* Initialize Shops		
		 * int index1, int itemID, int itemCost
		 * 
		 * Initialize Noob Shop
		 */
		
		Shop noobShop = new Shop();
		
		//Initialize Shop Items
		
		noobShop.setInventoryItem(0, 100, 50); //Health Potion Low
		noobShop.setInventoryItem(1, 101, 300); //Health Potion Medium
		noobShop.setInventoryItem(2, 102, 2000); //Health Potion High
		
		//Initialize Dungeons
		//int numMonsters, int levelMonsters, String Name
		
		Dungeon dungeon11 = new Dungeon(1, 1, "Your first room");
		
		//Initialize Combat
		Combat combat = new Combat();
		
		//Initialize Mobs
		Monster slime = new Monster("Slime", 20, 1, 2, 1, 0, 0);
		
		
		//Start Game
		System.out.println("Welcome to The RPG! \n");
		System.out.println("Type y to start and n to exit.");
		
		if (!menu.checkIfDone()){
			System.exit(0);
		}
		
		menu.initilizePlayer(player);
		
		menu.printIntro();
		
		boolean isRunning = true;
		
		
		//This is the game itself, It will run methods based off of where the player is at using player.getCurrentLocation()
		while(isRunning){
			switch(player.getCurrentLocation()){
			//TOWN: 0, SHOP: 1, Inventory: 2, Dungeon Selection: 3, Player Stats: 4 DUNGEON_1_1: 11, DUNGEON_1_2: 12, COMBAT: 100};
			case -1:
				isRunning = false;
				break;
			case 0:
				menu.printTownMenu(player);
				menu.printMovement(player);
				break;
			case 1:
				menu.printNoobTownShop(noobShop, player, itemDatabase);
				menu.printMovement(player);
				break;
			case 2:
				player.printInventory(itemDatabase);
				menu.printMovement(player);
				break;
			case 3:	
				menu.printDungeonSelection(player, dungeon11);
				if (player.getCurrentLocation() > 10){
					break;
				} else {
					menu.printMovement(player);
					break;
				}
			case 4:
				menu.printPlayerStats(player);
				menu.printMovement(player);
				break;
			case 11:
				combat.lookAround(dungeon11);
				if (combat.checkIfAttack() == true){
					combat.startCombat(dungeon11, player, slime, potionHealthLow);
				} else {
					player.setCurrentLocation(3);
				}
				
				break;
			case 12:
				break;
			case 100:
				break;
			}
		}
	}
}
