import java.util.Scanner;

public class Shop {

	private String _name;
	private int _gold;
	
	
	//Item ID, Cost
	private int[][] _inventory = new int[100][2];

	public String getName() {
		return _name;
	}

	public void setName(String _name) {
		this._name = _name;
	}

	public int getGold() {
		return _gold;
	}

	public void setGold(int _gold) {
		this._gold = _gold;
	}

	public int[][] getInventory() {
		return _inventory;
	}
	
	public void setInventoryItem(int index, int itemID, int itemCost) {
		this._inventory[index][0] = itemID;
		this._inventory[index][1] = itemCost;
	}
	
	public int getItemCost(int index){
		return this._inventory[index][1];
	}
	
	public void printInventory(int numberItems){
		//Prints the shops inventory 
		for (int index = 0; index < numberItems; index++){
			switch(getInventory()[index][0]){
			case 100:
				System.out.println("1) Health Potion Low: " + getInventory()[index][1] + " gold");
				break;
			case 101:
				System.out.println("2) Health Potion Medium: " + getInventory()[index][1] + " gold");
				break;
			case 102:
				System.out.println("3) Health Potion High: " + getInventory()[index][1] + " gold");
				break;
			}
		}
	}
	
	public void shopItem(Player player, ItemDatabase items){
		/*Used to buy a item from the shop
		 *
		 * */
		Scanner userInput = new Scanner(System.in);
		boolean isInvalidInput = true;
		boolean isInvalidInput2 = true;
		int input;
		
		while (isInvalidInput){
			System.out.println("Input the number corrisponing with the item. If you wish to buy. Enter -1 to exit.");
			System.out.println("You have " + player.getGold() + " Gold.");
            input = userInput.nextInt();
			switch(input){
			case -1:
				System.out.println("Exiting shop.");
				isInvalidInput = false;
				break;
			case 1:
				while (buyItem(player, 0, items.getItem(100), 100)){
				}
				isInvalidInput = checkIfLeave();
				break;
			case 2:
				while (buyItem(player, 1, items.getItem(101), 101)){
				}
				isInvalidInput = checkIfLeave();
				break;
			case 3:
				while (buyItem(player, 2, items.getItem(102), 102)){
				}
				isInvalidInput = checkIfLeave();
				break;
			default:
				break;
			}
		}
	}
	
	private boolean buyItem(Player player, int item, String name, int itemID){
		//Player follows on screen instructions to buy a item and put it in their inventory
		System.out.println("How many " +  name + " will you buy?");
		Scanner userInput = new Scanner(System.in);
		int input;
        input = userInput.nextInt();
        if ((input * getItemCost(item)) > player.getGold()){
        	System.out.println("That would cost you: " + (input*getItemCost(item)) + " gold. You do not have enough gold.");
        	return true;
        } else if ((input * getItemCost(item)) <= player.getGold() ){
        	player.setGold(player.getGold() - (input * getItemCost(item)));
        	player.addItem(itemID, input);
        	System.out.println("You purchased " + input + name);
        	return false;
        } else if(input == -1){
        	return false;
        }else{
        	System.out.println("Invalid input.");
        	return true;
        }
	}
	
	private boolean checkIfLeave(){
		//A true or false return method that gives some resistance to errors
		System.out.println("Do you want to buy anything else? y/n");
		Scanner userInput = new Scanner(System.in);
		boolean isValidInput = true;
		String input = "";
		
		while(isValidInput){
			boolean isInvalidInput = true;
			
			input = "n";
			
			while (isInvalidInput){
				input = userInput.next();
				
				if (input.length() > 1){
					System.out.println("Invalid input. Please enter a single char; n/N for no and y/Y for yes.");
					
				} else {
					switch(input.charAt(0)){
					case 'y':
					case 'Y':
						isInvalidInput = false;
						break;
					case 'n':
					case 'N':
						isInvalidInput = false;
						break;
					default:
						System.out.println("Invalid input. Please enter n/N for no and y/Y for yes.");
						break;
					}
				}
			}

			switch(input.charAt(0)){
				case 'y':
				case 'Y':
					return true;
				case 'n':
				case 'N':
					return false;
			default:
					return false;
			}
		}
		return false;
	}
}
