
public class Player {
	//Used to store all information about the player and related methods

	private String _name;
	private int _ID;
	private int _HP;
	private int _maxHP;
	private int _MP;
	private int _maxMP;
	
	private int _attack;
	private int _defense;
	private int _magicAttack;
	private int _magicDefense;
	
	private int _gold;
	
	private int[][] _inventory = new int[64][2];
	
	//TOWN: 0, SHOP: 1, Inventory: 2, Dungeon Selection: 3, Shop: 4, Player Stats: 5 DUNGEON_1_1: 11, DUNGEON_1_2: 12, COMBAT: 100};
	private int _currentLocation = 0;
	private int _lastLocation = 0;
	
	Player(){
		setID(1);
		
		setHP(100);
		setMaxHP(100);
		
		setMP(10);
		setMaxMP(10);
		
		setAttack(5);
		setDefense(5);
		
		setMagicAttack(5);
		setMagicDefense(5);
		
		setGold(100);
		
		int[][] tempArray = getInventory();
		for (int x = 0; x < getInventory().length; x++ ){
			for (int y = 0; y < getInventory()[0].length; y++){
				tempArray[x][y] = -1;
			}
		}
		
	}
	
	//Getters
	
	public String getName() {
		return _name;
	}
	
	public int getID() {
		return _ID;
	}
	
	public int getHP() {
		return _HP;
	}
	
	public int getMaxHP() {
		return _maxHP;
	}
	
	public int getAttack() {
		return _attack;
	}
	
	public int getDefense() {
		return _defense;
	}
	
	public int getMagicAttack() {
		return _magicAttack;
	}
	
	public int getMagicDefense() {
		return _magicDefense;
	}
	
	public int getGold() {
		return _gold;
	}
	

	//Setters
	public void setName(String _name) {
		this._name = _name;
	}

	public void setID(int _ID) {
		this._ID = _ID;
	}

	public void setHP(int _HP) {
		this._HP = _HP;
	}
	
	public void setMaxHP(int _maxHP) {
		this._maxHP = _maxHP;
	}

	public void setAttack(int _attack) {
		this._attack = _attack;
	}

	public void setDefense(int _defense) {
		this._defense = _defense;
	}

	public void setMagicAttack(int _magicAttack) {
		this._magicAttack = _magicAttack;
	}

	public void setMagicDefense(int _magicDefense) {
		this._magicDefense = _magicDefense;
	}

	public void setGold(int _gold) {
		this._gold = _gold;
	}

	//Magic power not used left in for future use
	public int getMP() {
		return _MP;
	}

	public void setMP(int _MP) {
		this._MP = _MP;
	}

	public int getMaxMP() {
		return _maxMP;
	}

	public void setMaxMP(int _maxMP) {
		this._maxMP = _maxMP;
	}
	
	//Locations

	public int getCurrentLocation() {
		return _currentLocation;
	}

	public void setCurrentLocation(int _currentLocation) {
		this._currentLocation = _currentLocation;
	}

	public int getLastLocation() {
		return _lastLocation;
	}

	public void setLastLocation(int _lastLocation) {
		this._lastLocation = _lastLocation;
	}

	//Inventory
	public int[][] getInventory() {
		return _inventory;
	}

	public void setInventory(int[][] _inventory) {
		this._inventory = _inventory;
	}
	
	public void setInventoryIndex(int index1, int index2, int value) {
		this._inventory[index1][index2] = value;
	}
	
	public void addItem(int itemID, int number){
		//Adds a item(s) to the players inventory
		for (int x = 0; x < getInventory().length; x++ ){
			if ((getInventory()[x][0] == -1) && (getInventory()[x][1] == -1)){
				setInventoryIndex(x, 0, itemID);
				setInventoryIndex(x, 1, number);
				break;
			} else if (itemID == getInventory()[x][0]) {
				setInventoryIndex(x, 1, (getInventory()[x][1] + number));
			}
		}
	}
	
	public void printInventory(ItemDatabase items){
		//Prints out the player's inventory
		for (int x = 0; x < getInventory().length; x++ ){
			//System.out.println("Inventory: ITEMID: " + getInventory()[x][0] + " NUMBER: " + getInventory()[x][1]); DEBUG
			if ((getInventory()[x][0] == -1) && (getInventory()[x][1] == -1)){
				System.out.println("End of inventory.");
				break;
			}
			switch(getInventory()[x][0]){
			case 100:
				System.out.println(items.getItem(100) + " " + getInventory()[x][1]);
				break;
			case 101:
				System.out.println(items.getItem(101) + " " + getInventory()[x][1]);
				break;
			case 102:
				System.out.println(items.getItem(102) + " " + getInventory()[x][1]);
				break;
			} 
			
		}
	}
}
