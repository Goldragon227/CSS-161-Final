
public class Item {
	//A class that stores information about a item and provides methods related to the item.
	private String _name;
	private int _ID;
	
	private int _attack;
	private int _defense;
	private int _magicAttack;
	private int _magicDefense;
	
	private int _restoreHPValue;
	private int _restoreMPValue;
	
	public Item(String name, int ID, int attack, int defense, int magicAttack, int magicDefense){
		
		setName(name);
		setID(ID);
		setAttack(attack);
		setDefense(defense);
		
		//Magic not used in-game at the moment
		setMagicAttack(magicAttack);
		setMagicDefense(magicDefense);
	}
	
	public Item(String name, int ID,  int restoreHPValue){
		setName(name);
		setID(ID);
		setRestoreHPValue(restoreHPValue);
	}
	
	public String getName() {
		return _name;
	}
	
	public int getID() {
		return _ID;
	}
	
	public int getAttack() {
		return _attack;
	}
	
	public int getDefense() {
		return _defense;
	}
	
	public int getMagicAttack() {
		return _magicAttack;
	}
	
	public int getMagicDefense() {
		return _magicDefense;
	}
	
	public int getRestoreHPValue() {
		return _restoreHPValue;
	}
	
	public int getRestoreMPValue() {
		return _restoreMPValue;
	}
	

	public void setName(String _name) {
		this._name = _name;
	}

	public void setID(int _ID) {
		this._ID = _ID;
	}
	
	public void setAttack(int _attack) {
		this._attack = _attack;
	}

	public void setDefense(int _defense) {
		this._defense = _defense;
	}

	public void setMagicAttack(int _magicAttack) {
		this._magicAttack = _magicAttack;
	}

	public void setMagicDefense(int _magicDefense) {
		this._magicDefense = _magicDefense;
	}

	public void setRestoreHPValue(int _restoreHPValue) {
		this._restoreHPValue = _restoreHPValue;
	}

	public void setRestoreMPValue(int _restoreMPValue) {
		this._restoreMPValue = _restoreMPValue;
	}
	
	public int restorePlayerHealth(Player player){
		//TODO
		
		if ((player.getHP() + getRestoreHPValue()) > player.getMaxHP()){
			player.setHP(player.getMaxHP());
			return 0;
			//Full HP fill
		} else if ((player.getHP() + getRestoreHPValue()) < player.getMaxHP()) {
			player.setHP(player.getHP() + getRestoreHPValue());
			return 1;
			//Partial fill
		} else if (player.getHP() == player.getMaxHP()){
			//You cannot use a HP pot
			return 2;
		}
		
		return -1;
	}
	
	public int restoreMonsterHealth(Monster monster){
		//TODO
		if ((monster.getHP() + getRestoreHPValue()) > monster.getMaxHP()){
			monster.setHP(monster.getMaxHP());
			return 0;
			//Full HP fill
		} else if ((monster.getHP() + getRestoreHPValue()) < monster.getMaxHP()) {
			monster.setHP(monster.getHP() + getRestoreHPValue());
			return 1;
			//Partial fill
		} else if (monster.getHP() == monster.getMaxHP()){
			//You cannot use a HP pot
			return 2;
		}
		
		return -1;
	}
	
	//Magic power not used left in for future use
	public void restorePlayerMP(Player player){
		if ((player.getMP() + getRestoreHPValue()) > player.getMaxMP()){
			
		} else if ((player.getMP() + getRestoreHPValue()) < player.getMaxMP()) {
			
		} else if (player.getMP() == player.getMaxMP()){
			
		}
	}
	
	public void restoreMonsterMP(){
		
	}
}
