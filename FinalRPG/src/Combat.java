import java.util.Scanner;

public class Combat {
	//Stores relevant methods for the combat system
	
	public void lookAround(Dungeon dungeon){
		//Prints to console how many monsters are in the room
		if (dungeon.getNumMonsters() > 0){
			System.out.println("There are " + dungeon.getNumMonsters() + " in the room.");
		} else {
			System.out.println("There are no monsters in the room.");
		}
	}
	
	public boolean checkIfAttack(){
		System.out.println("Do you want to engage? y/n");
		return checkYN();
		
	}
	
	private void printBattleUI(Player player, Monster monster){
		System.out.println(player.getName() + "    Vs    " + monster.getName());
		System.out.print("HP: " + player.getHP() + " / " + player.getMaxHP() + "  |  ");
		System.out.println("HP: " + monster.getHP() + " / " + monster.getMaxHP());
		//System.out.print("MP: " + player.getMP() + " / " + player.getMaxMP() + "  |  ");
	}
	
	public void startCombat(Dungeon dungeon, Player player, Monster slime, Item potionLow) {
		//The main combat driver that will run until either the player dies or the monster
		boolean isCombat = true;
		char input;
		Scanner userInput = new Scanner(System.in);
		boolean isValidInput = true;
		
		
		//String name, int HP, int attack, int defense, int magicAttack, int magicDefense
		
		
		
		while (isCombat){
			printBattleUI(player, slime);
			
			System.out.println("A: Attack, R: Run Away");
			input = userInput.next().charAt(0);
			switch (input){
			case 'a':
			case 'A':
				playerAttack(player, slime);
				monsterAttack(player, slime);
				break;
			case 'r':
			case 'R':
				System.out.println(player.getName() + " ran away!");
				isCombat = false;
				break;
			}
			
			
			isCombat = checkIfPlayerAlive(player);
			isCombat = checkIfMonsterAlive(slime);
		}
		
		if (checkIfPlayerAlive(player) && (checkIfMonsterAlive(slime) == false)){
			resetMonster(slime);
			player.setGold(player.getGold() + slime.getGoldDrop());
		} else if (!checkIfPlayerAlive(player)) {
			resetPlayer(player);
			resetMonster(slime);
		} else {
			resetMonster(slime);
		}
	}
	
	private void playerAttack(Player player, Monster monster){
		//Gets the players attack and the monsters defense and deals damage to the monster
		int damage = player.getAttack() - monster.getDefense();
		System.out.println(player.getName() + " attacks " + monster.getName() + " for " + damage + " damage. ");
		monster.setHP(monster.getHP() - damage);
	}
	
	private void monsterAttack(Player player, Monster monster){
		//Gets the monsters attack and the players defense and deals damage to the player
		int damage = monster.getAttack();
		System.out.println(monster.getName() + " attacks " + player.getName() + " for " + damage + " damage. ");
		player.setHP(player.getHP() - damage);
	}
	
	private boolean checkIfPlayerAlive(Player player){
		if (player.getHP() <= 0){
			return false;
		} else {
			return true;
		}
		
	}
	
	private boolean checkIfMonsterAlive(Monster monster){
		if (monster.getHP() <= 0){
			return false;
		} else {
			return true;
		}
	}
	
	private void resetPlayer(Player player){
		//Resets the player to town
		System.out.println("Player has died! Returning to town.");
		player.setCurrentLocation(0);
		player.setLastLocation(0);
		player.setHP(player.getMaxHP());
		player.setMP(player.getMaxMP());
	}
	
	private void resetMonster(Monster monster){
		monster.setHP(monster.getMaxHP());
	}
	
	private boolean checkYN(){
		//A true or false return method that gives some resistance to errors
		Scanner userInput = new Scanner(System.in);
		boolean isValidInput = true;
		String input = "";
		
		while(isValidInput){
			boolean isInvalidInput = true;
			
			input = "n";
			
			while (isInvalidInput){
				input = userInput.next();
				
				if (input.length() > 1){
					System.out.println("Invalid input. Please enter a single char; n/N for no and y/Y for yes.");
					
				} else {
					switch(input.charAt(0)){
					case 'y':
					case 'Y':
						isInvalidInput = false;
						break;
					case 'n':
					case 'N':
						isInvalidInput = false;
						break;
					default:
						System.out.println("Invalid input. Please enter n/N for no and y/Y for yes.");
						break;
					}
				}
			}

			switch(input.charAt(0)){
				case 'y':
				case 'Y':
					return true;
				case 'n':
				case 'N':
					return false;
			default:
					return false;
			}
		}
		return false;
	}

}
